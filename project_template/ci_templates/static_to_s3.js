var AWS = require('aws-sdk')
var exec = require('child_process').exec;
var fs = require('fs')

// The following variables should be in environment:
// AWS_ACCESS_KEY_ID
// AWS_SECRET_ACCESS_KEY
// AWS_BUCKET_NAME

AWS.config.update({ accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY });

// Read in the file, convert it to base64, store to S3
const child = exec('tar cz -C dist .', {maxBuffer:100500100, encoding: 'buffer'}, (error, stdout, stderr) => {
	if (error) {
		console.error('stderr', stderr);
		throw error;
	}
	var s3 = new AWS.S3();
	s3.putObject({
		ACL: 'public-read',
		Bucket: process.env.AWS_BUCKET_NAME,
		Key: 'compiled/frontend_static.tar.gz',
		Body: stdout
	}, function (error, resp) {
		console.log(error);
	});
});
