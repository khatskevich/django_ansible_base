#!/usr/bin/env python3
import subprocess as sp
import boto3
import os

# The following variables should be in environment:
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_BUCKET_NAME

# it is easy to pass secrets with any CI using ENV variables
env = os.environ

# cmd to collectstatic
sub_env = env.copy()
sub_env["DJANGO_SETTINGS_MODULE"] = "config.settings.local"
manage = sp.run(["./manage.py", "collectstatic", "--no-input"], env=sub_env)

# create tar.gz archive in-memory
static_tar_gz = sp.check_output("tar cz -C staticfiles/ .".split())

# Create an S3 client
s3 = boto3.client(
    's3',
    region_name="eu-central-1",
    aws_access_key_id=env["AWS_ACCESS_KEY_ID"],
    aws_secret_access_key=env["AWS_SECRET_ACCESS_KEY"])

bucket_name = env["AWS_BUCKET_NAME"]
filename = 'compiled/django_static.tar.gz'
s3.put_object(
    Body=static_tar_gz,
    Bucket=bucket_name,
    Key=filename,
    ACL='public-read')
