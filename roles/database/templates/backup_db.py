#!/usr/bin/env python3

import boto3
import subprocess
from io import BytesIO
import zipfile
from datetime import datetime, timedelta, timezone
import logging

logger = logging.getLogger(__name__)

databases = [
    '{{project_name}}',
    {% if pg_extra_databases is defined %}{% for database in pg_extra_databases %}
    '{{database.database}}',
    {% endfor %}{% endif %}
]
BUCKET_NAME = '{{django_env.AWS_BUCKET_NAME}}'
PROJECT_NAME = '{{project_name}}'

# Create an S3 client
s3 = boto3.client(
    's3',
    region_name="eu-central-1",
    aws_access_key_id="{{django_env.AWS_ACCESS_KEY_ID}}",
    aws_secret_access_key="{{django_env.AWS_SECRET_ACCESS_KEY}}")

for database in databases:
    timestamp = datetime.now().strftime('%Y_%m_%d_%H_%M')
    filename = f'backups/{database}_{timestamp}.sql.gz'
    data = subprocess.check_output([
        'pg_dump',
        '-Z', '6',
        '-d',
        database])
    s3.put_object(Body=data, Bucket=BUCKET_NAME, Key=filename)

### DELETE OLD BACKUPS ###

# Dict structure (days_back_since, days_back_till): backups_count
PERIODS = {
    (0, 7): None,
    (7, 14): 3,
    (14, 30): 3,
    (30, 60): 3,
    (60, 90): 3,
    (90, 180): 3,
    (180, 360): 2,
    (360, 360 * 10): None
}


def group_content(sorted_content):
    grouped = dict()
    for p in PERIODS.keys():
        group_items = []
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        since = now - timedelta(days=p[0])
        till = now - timedelta(days=p[1])

        for idx, content in enumerate(sorted_content):
            last_modified = content['LastModified'].replace(tzinfo=timezone.utc)

            if since > last_modified >= till:
                group_items.append(sorted_content[idx])
        grouped[p] = group_items
    return grouped


def sort_content(objs):
    try:
        sorted_content = sorted(objs['Contents'], key=lambda c_: c_['LastModified'])
    except KeyError:
        logger.error('No content found')
        raise
    return sorted_content


def determine_deletable(grouped_content):
    to_delete = []

    for since_till, content_list in grouped_content.items():
        # count - number of objects that should not be deleted
        count = PERIODS[since_till]

        if count:
            # count - num of objects to preserve
            if len(content_list) > count:
                for c in content_list[count:]:
                    to_delete.append(c['Key'])
    return to_delete


def delete_objects(bucket, keys_to_delete):
    if not keys_to_delete:
        return
    s3.delete_objects(
        Bucket=bucket,
        Delete={
            'Objects': [
                {'Key': value} for value in keys_to_delete
            ]
        }
     )


def delete_old_backups(bucket):
    objects = s3.list_objects_v2(Bucket=bucket, Prefix=f'backups/{PROJECT_NAME}')
    sorted_objects = sort_content(objects)
    grouped_objects = group_content(sorted_objects)
    deletable_keys = determine_deletable(grouped_objects)
    delete_objects(bucket, deletable_keys)


delete_old_backups(BUCKET_NAME)
