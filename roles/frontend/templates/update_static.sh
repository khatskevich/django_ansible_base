#!/bin/sh
cd {{static_path}}

# Check if django static is updated
# stat would work even if file do not exist
mod_time1=$(stat django_static.tar.gz --format="%Y")
wget {{AWS_STATIC_URL}}/django_static.tar.gz -N
mod_time2=$(stat django_static.tar.gz --format="%Y")
if [ "$mod_time1" != "$mod_time2" ]
then
	rm -rf {{django_static_path}}/*
	tar -xzf django_static.tar.gz -C {{django_static_path}}
fi

# The same with afrontend
{% if node_apps is defined %} {% for dynsite in node_apps %}
mod_time1=$(stat {{dynsite.archive_name}} --format="%Y")
wget {{dynsite.archive_url + dynsite.archive_name}} -N
mod_time2=$(stat {{dynsite.archive_name}} --format="%Y")
if [ "$mod_time1" != "$mod_time2" ]
then
	# Crutch: do not delete node modules.
	sh -c 'cd {{static_path + dynsite.path}} && rm -rf $(ls -A -I node_modules)'
	tar -xzf {{dynsite.archive_name}} -C {{static_path + dynsite.path}}
	cd {{static_path + dynsite.path}} && npm i
fi
killall node || echo "Node is not running"
{% endfor %} {% endif %}

{% if static_sites is defined %}{% for site in static_sites %}
{% if is_prod %}
FNAME={{site.file_name}}.tar.gz
{% else %}
FNAME=staging_{{site.file_name}}.tar.gz
{% endif %}
mod_time1=$(stat $FNAME --format="%Y")
wget {{AWS_STATIC_URL}}/$FNAME -N
mod_time2=$(stat $FNAME --format="%Y")
if [ "$mod_time1" != "$mod_time2" ]
then
	# Crutch: do not delete node modules.
	sh -c 'cd {{static_path}}/{{site.file_name}} && rm -rf $(ls -A -I node_modules)'
	tar -xzf $FNAME -C {{static_path}}/{{site.file_name}}/
	cd {{static_path}}/{{site.file_name}}/ && npm i
fi
{% endfor %}{% endif %}
